﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.GASOLINERIA.LIBRARY
{
    public enum Modelos { redCar, blueCar, greenCar};
    public enum EstadosCarro { Llenando, Moviendose, Esperando, MoviendoseEnCola, DeSalida, DeEntrada}

    public class Carro
    {
        public int carroID { get; set; }
        public int colaID { get; set; }
        public Modelos modelo { get; set; }
        public EstadosCarro estado { get; set; }
        public int coordX { get; set; }
        public int coordY { get; set; }
        public int tiempo { get; set; }

        public Combustible combustible;

        public Carro(Random aleatorio)
        {
            int random = aleatorio.Next(3);

            if (random == 0)
            {
                combustible = new Combustible(Combustibles.Gasolina);
            }
            else if (random == 1)
            {
                combustible = new Combustible(Combustibles.Petroleo);
            }
            else if (random == 2)
            {
                combustible = new Combustible(Combustibles.GNV);
            }

            if (combustible.tipo == Combustibles.Gasolina)
            {
                random = aleatorio.Next(3);

                if (random == 0)
                    combustible.octanaje = 90;
                else if (random == 1)
                    combustible.octanaje = 95;
                else if (random == 2)
                    combustible.octanaje = 97;
            }

            combustible.cantidad = aleatorio.Next(10, 100) + (aleatorio.Next(0, 100)) / 100f;

            tiempo = tiempoPorCarro();
            estado = EstadosCarro.Moviendose;
            random = aleatorio.Next(Enum.GetNames(typeof(Modelos)).Length);
            modelo = (Modelos)random;
        }

        public Carro(Combustibles pTipo, float pCantidad)
        {
            //Constructor para crear un carro que consume GNV o Petroleo
            combustible = new Combustible(pTipo, pCantidad);
        }
        public Carro(float pCantidad,int pOctanaje)
        {
            //Constructor para crear un carro que consume gasolina
            combustible = new Combustible(pCantidad, pOctanaje);
        }
        public Carro()
        {
            carroID = -1;
            colaID = -1;
            modelo = Modelos.redCar;
            coordX = 0;
            coordY = 0;
            combustible = new Combustible();
            
        }
        
        public int tiempoPorCarro()
        {
            int tiempo = (int)(combustible.cantidad / 2);

            if (tiempo < 25)
                tiempo = 25;

            return tiempo + 50;
        }
    }
}
