﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.GASOLINERIA.LIBRARY
{
    public delegate void delegadoVoid();

    public enum EstadosCola { Moviendose, Atendiendo, Libre, Vacia }

    public class ColaDeCarros
    {
        public delegadoVoid onAtencion;
        public EstadosCola estado { get; set; }
        public int colaID { get; set; }
        public int coordX { get; set; }
        public int coordY { get; set; }
        public int ultimoX { get; set; }
        public int atencionX { get; set; }
        public int currentCarroID { get; set; }
        public List<Carro> cola { get; set; }
        public Combustibles tipo { get; set; }
        public float totalDinero { get; set; }
        public float totalCantidad { get; set; }

        public ColaDeCarros(int pColaID, Combustibles pTipo)
        {
            currentCarroID = -1;

            totalDinero = 0;
            totalCantidad = 0;

            estado = EstadosCola.Vacia;
            colaID = pColaID;
            cola = new List<Carro>();
            tipo = pTipo;
            if (esColaArriba())
                atencionX = 220;
            else
                atencionX = 600;
        }

        public void setCoordCarro(Carro pCarro)
        {
            if (esColaArriba())
            {
                pCarro.coordX = -80;          // pCarro.coordX = coordX - 80 - 90 * pLugar;          //
                pCarro.coordY = coordY - 40;                        //
            }                                                       //
            else                                                    //
            {                                                       //
                pCarro.coordX = 900;         //  pCarro.coordX = coordX + 300 + 90 * pLugar;  
                pCarro.coordY = coordY + 70;                        //  pCarro.coordY = coordY + 70;                 
            }
        }

        public void addCarro(Carro pCarro)
        {
            if (currentCarroID == -1)
                currentCarroID = 0;

            if (esColaArriba())
            {
                pCarro.coordX = -200;
                ultimoX = coordX - 80 - 90 * cola.Count;
            }
            else
            {
                pCarro.coordX = 900;
                ultimoX = coordX + 300 + 90 * cola.Count;
            }

            setCoordCarro(pCarro);
            cola.Add(pCarro);

        }


        public bool esColaArriba()
        {
            if (colaID % 2 == 0)
                return false;
            else
                return true;
        }

        public void setCoordCarros()
        {
            for (int i = 0; i < cola.Count; i++)
            {
                setCoordCarro(cola[i]);
            }
        }

        public void getNextCarroID()
        {
            currentCarroID = -1;
            for (int i = 0; i < cola.Count; i++)
            {
                if (cola[i].estado != EstadosCarro.DeSalida)
                {
                    currentCarroID = cola[i].carroID;
                    cola[i].estado = EstadosCarro.Moviendose;
                }

            }
            if (currentCarroID == -1)
                estado = EstadosCola.Vacia;
        }

        public void AtenderCarro()
        {
            cola[currentCarroID].tiempo = cola[currentCarroID].tiempo - 1;

            if (cola[currentCarroID].tiempo == 0)
            {
                totalDinero += cola[currentCarroID].combustible.ObtenerPrecio();
                totalCantidad += cola[currentCarroID].combustible.cantidad;
                onAtencion();
                cola[currentCarroID].estado = EstadosCarro.DeSalida;
                estado = EstadosCola.Moviendose;
                getNextCarroID();
            }

        }

        public void MoverCola()
        {
            bool carroDeSalida = false;
            int aux = 0;

            if (cola.Count != 0)
            {
                for (int i = 0; i < cola.Count; i++)
                {
                    if (cola[i].estado == EstadosCarro.Moviendose)
                    {
                        cola[i].coordX = cola[i].coordX + 10;
                        if (cola[i].coordX == atencionX - (i - currentCarroID) * 90)
                            cola[i].estado = EstadosCarro.Esperando;
                    }
                    else if (cola[i].estado == EstadosCarro.DeSalida)
                    {
                        cola[i].coordX = cola[i].coordX + 10;
                        carroDeSalida = true;
                        aux = i;
                    }
                }

                if (cola[currentCarroID].coordX == atencionX)
                {
                    cola[currentCarroID].estado = EstadosCarro.Llenando;
                    estado = EstadosCola.Atendiendo;
                }

                if (carroDeSalida && (cola[aux].coordX == 910 || cola[aux].coordX == -90))
                {
                    cola.RemoveAt(aux);
                    getNextCarroID();
                }
            }

        }

    }
}