﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.GASOLINERIA.LIBRARY
{
    public enum EstadosIsla { Operando, Vacia}
    public class Isla
    {
        public int islaID { get; set; }
        public int coordX { get; set; }
        public int coordY { get; set; }
        public float cantidad { get; set; }
        public float dinero { get; set; }
        public EstadosIsla estado;
        public Combustibles tipo;
        public ColaDeCarros colaArriba;
        public ColaDeCarros colaAbajo;

        public Isla(Combustibles pTipo, int pColaArribaID, int pColaAbajoID, int pCoordX, int pCoordY)
        {
            tipo = pTipo;
            dinero = 0;
            cantidad = 1000;

            coordX = pCoordX;
            coordY = pCoordY;
            colaArriba = new ColaDeCarros(pColaArribaID, tipo);
            colaAbajo = new ColaDeCarros(pColaAbajoID, tipo);

            estado = EstadosIsla.Operando;

            colaArriba.coordX = coordX;
            colaArriba.coordY = coordY;

            colaAbajo.coordX = coordX;
            colaAbajo.coordY = coordY;
        }

        //Retorna el numero de carros de su cola más vacia
        public int retornartMinimoDeColas()
        {
            Random r = new Random((int)DateTime.Now.Ticks);
            //Si la cola dereche tiene menos carros que la cola Abajo...
            if (colaArriba.cola.Count < colaAbajo.cola.Count)
            {
                return colaArriba.cola.Count;
            }
            //...O si la cola Abajo Abajo tiene menos carros...
            else if (colaAbajo.cola.Count < colaArriba.cola.Count)
            {
                return colaAbajo.cola.Count;
            }
            //...O si no hay ningun carro, o si hay el mismo numero de carros...
            else if (r.Next(2) == 0)
                return colaArriba.cola.Count;
            else
                return colaAbajo.cola.Count;
        }

        //Retorna la cola con menos carros y si no hay elige una al azar
        public ColaDeCarros RetornarColaLibre()
        {
            Random r = new Random((int)DateTime.Now.Ticks);
            //Si la cola dereche tiene menos carros que la cola Abajo...
            if (colaArriba.cola.Count < colaAbajo.cola.Count)
            {
                return colaArriba;
            }
            //...O si la cola Abajo Abajo tiene menos carros...
            else if (colaAbajo.cola.Count < colaArriba.cola.Count)
            {
                return colaAbajo;
            }
            //...O si no hay ningun carro, o si hay el mismo numero de carros...
            else if (r.Next(2) == 0)
                return colaArriba;
            else
                return colaAbajo;
        }

        public void atualizarValores()
        {
            dinero = colaArriba.totalDinero + colaAbajo.totalDinero;
            cantidad = cantidad -  colaAbajo.totalCantidad - colaArriba.totalCantidad;

            if (cantidad < 0)
                cantidad = 0;
        }
    }   
}
