﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UPC.GASOLINERIA.LIBRARY
{
    public enum Combustibles { Gasolina, Petroleo, GNV };
   
    public class Combustible
    {
        public Combustibles tipo;
        public int octanaje { get; set; }
        public float cantidad { get; set; }
        public float precio { get; set; }

        public Combustible()
        {
            tipo = Combustibles.Gasolina;
            cantidad = 0;
            precio = 0;
            octanaje = 90;
        }

        public Combustible(Combustibles pTipo, float pCantidad)
        {
            tipo = pTipo;
            cantidad = pCantidad;
            precio = 0;
            octanaje = 0;
        }
        public Combustible(float pCantidad, int pOctanaje)
        {
            tipo = Combustibles.Gasolina;
            cantidad = pCantidad;
            precio = 0;
            octanaje = pOctanaje;
        }
        public Combustible(Combustibles pTipo)
        {
            tipo = pTipo;
            precio = 0;
            octanaje = 0;
        }
        public float ObtenerPrecio()
        {
            if (tipo == Combustibles.Gasolina)
            {
                if (octanaje == 90)
                    precio = 13.50f * cantidad;

                else if (octanaje == 95)
                    precio = 15.50f * cantidad;

                else if (octanaje == 97)
                    precio = 16.50f * cantidad;
            }
            else if (tipo == Combustibles.Petroleo)
                precio = 12f * cantidad;

            else
                precio = 1.80f * cantidad;

            return precio;
        }

    }
}
