﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using UPC.GASOLINERIA.LIBRARY;

namespace UPC.GASOLINERIA.UI
{
    public class Dibujos
    {
        public Simulator simulator { get; set; }

        public Dibujos(Simulator pSimulator)
        {
            simulator = pSimulator;
        }

        public void PintarOctanaje(ColaDeCarros pCola, int pOctanaje = 0,bool render = false)
        {
            int relativeX;
            int relativeY;

            if (pCola.esColaArriba())
            {
                relativeX = pCola.coordX;
                relativeY = pCola.coordY;
            }
            else
            {
                relativeX = pCola.coordX + 270;
                relativeY = pCola.coordY;
            }

            simulator.buffer.Graphics.FillRectangle(new SolidBrush(Color.White), new Rectangle(relativeX, relativeY + 3, 30, 70));

            if (pOctanaje == 90)
                simulator.buffer.Graphics.FillRectangle(new SolidBrush(Color.GreenYellow), new Rectangle(relativeX, relativeY + 3, 30, 20));
            else if(pOctanaje == 95)
                simulator.buffer.Graphics.FillRectangle(new SolidBrush(Color.GreenYellow), new Rectangle(relativeX, relativeY + 25, 30, 20));
            else if(pOctanaje == 97)
                simulator.buffer.Graphics.FillRectangle(new SolidBrush(Color.GreenYellow), new Rectangle(relativeX, relativeY + 47, 30, 20));

            simulator.buffer.Graphics.DrawRectangle(new Pen(new SolidBrush(Color.Black)), new Rectangle(relativeX, relativeY + 3, 30, 20));
            simulator.buffer.Graphics.DrawString("90", new Font("Arial", 12), new SolidBrush(Color.Black), new Point(relativeX + 2, relativeY + 5));
            simulator.buffer.Graphics.DrawRectangle(new Pen(new SolidBrush(Color.Black)), new Rectangle(relativeX, relativeY + 25, 30, 20));
            simulator.buffer.Graphics.DrawString("95", new Font("Arial", 12), new SolidBrush(Color.Black), new Point(relativeX + 2, relativeY + 27));
            simulator.buffer.Graphics.DrawRectangle(new Pen(new SolidBrush(Color.Black)), new Rectangle(relativeX, relativeY + 47, 30, 20));
            simulator.buffer.Graphics.DrawString("97", new Font("Arial", 12), new SolidBrush(Color.Black), new Point(relativeX + 2, relativeY + 49));

            if (render)
                simulator.buffer.Render(simulator.graphics);
        }

        public void PintarIsla(Isla pIsla, bool render = false)
        {
            simulator.buffer.Graphics.FillRectangle(new SolidBrush(Color.DodgerBlue), new Rectangle(pIsla.coordX, pIsla.coordY, 300, 70));
            simulator.buffer.Graphics.DrawString("S\\." + pIsla.dinero.ToString(), new Font("Arial", 16, FontStyle.Bold), new SolidBrush(Color.White), new Point(pIsla.coordX + 120, pIsla.coordY + 10));
            simulator.buffer.Graphics.DrawString( pIsla.tipo.ToString()+ " : " + pIsla.cantidad.ToString() + "Lts", new Font("Arial", 16,FontStyle.Bold), new SolidBrush(Color.White), new Point(pIsla.coordX + 50, pIsla.coordY + 40));

            if (pIsla.tipo == Combustibles.Gasolina)
            {
                PintarOctanaje(pIsla.colaArriba);
                PintarOctanaje(pIsla.colaAbajo);
            }

            if (render)
                simulator.buffer.Render(simulator.graphics);
        }

        public void PintarGrifo(bool render = false)
        {
            simulator.buffer.Graphics.Clear(Color.White);

            PintarIsla(simulator.islaG1);
            PintarIsla(simulator.islaG2);
            PintarIsla(simulator.islaPe);
            PintarIsla(simulator.islaGn);

            if (render)
                simulator.buffer.Render(simulator.graphics);
        }
        public void PintarCarro(Carro carro, bool render = false)
        {
            
            if (!simulator.getColaByID(carro.colaID).esColaArriba())
                simulator.carsImages[(int)carro.modelo].RotateFlip(RotateFlipType.Rotate180FlipY);

            simulator.buffer.Graphics.DrawImage(simulator.carsImages[(int)carro.modelo], new Rectangle(carro.coordX, carro.coordY, 80, 40), new Rectangle(0, 0, 800, 397), GraphicsUnit.Pixel);

            if (!simulator.getColaByID(carro.colaID).esColaArriba())
                simulator.carsImages[(int)carro.modelo].RotateFlip(RotateFlipType.Rotate180FlipY);

            if (render)
                simulator.buffer.Render(simulator.graphics);          
        }

        public void PintarCola(ColaDeCarros cola, bool render = false)
        {
            int relativeX, relativeY;

            if (cola.esColaArriba())
            {
                relativeX = 0;
                relativeY = 75 * cola.colaID - 75;
            }
            else
            {
                relativeX = 0;
                relativeY = 75 * cola.colaID - 40;
            }

            simulator.buffer.Graphics.FillRectangle(new SolidBrush(Color.White), new Rectangle(relativeX, relativeY, 900, 40));

            for (int i = 0; i < cola.cola.Count; i++)
            {
                PintarCarro(cola.cola[i]);
            }

            if (render)
                simulator.buffer.Render(simulator.graphics);
        }

        public void PintarCarros(bool render = false)
        {
            PintarCola(simulator.islaG1.colaArriba);
            PintarCola(simulator.islaG1.colaAbajo);
            PintarCola(simulator.islaG2.colaArriba);
            PintarCola(simulator.islaG2.colaAbajo);
            PintarCola(simulator.islaPe.colaArriba);
            PintarCola(simulator.islaPe.colaAbajo);
            PintarCola(simulator.islaGn.colaArriba);
            PintarCola(simulator.islaGn.colaAbajo);
            
            if (render)
                simulator.buffer.Render(simulator.graphics);
        }

        public void MoverCola(ColaDeCarros cola, bool render)
        {
            cola.cola.RemoveAt(0);
        }
    }
}
