﻿namespace UPC.GASOLINERIA.UI
{
    partial class Simulator
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.temporizador = new System.Windows.Forms.Timer(this.components);
            this.panel = new System.Windows.Forms.Panel();
            this.timerCola1 = new System.Windows.Forms.Timer(this.components);
            this.timerCola2 = new System.Windows.Forms.Timer(this.components);
            this.timerCola3 = new System.Windows.Forms.Timer(this.components);
            this.timerCola4 = new System.Windows.Forms.Timer(this.components);
            this.timerCola5 = new System.Windows.Forms.Timer(this.components);
            this.timerCola6 = new System.Windows.Forms.Timer(this.components);
            this.timerCola7 = new System.Windows.Forms.Timer(this.components);
            this.timerCola8 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // temporizador
            // 
            this.temporizador.Enabled = true;
            this.temporizador.Tick += new System.EventHandler(this.temporizador_Tick);
            // 
            // panel
            // 
            this.panel.Location = new System.Drawing.Point(12, 48);
            this.panel.Name = "panel";
            this.panel.Size = new System.Drawing.Size(900, 600);
            this.panel.TabIndex = 0;
            // 
            // timerCola1
            // 
            this.timerCola1.Enabled = true;
            this.timerCola1.Tick += new System.EventHandler(this.timerCola1_Tick);
            // 
            // timerCola2
            // 
            this.timerCola2.Enabled = true;
            this.timerCola2.Tick += new System.EventHandler(this.timerCola2_Tick);
            // 
            // timerCola3
            // 
            this.timerCola3.Enabled = true;
            this.timerCola3.Tick += new System.EventHandler(this.timerCola3_Tick);
            // 
            // timerCola4
            // 
            this.timerCola4.Enabled = true;
            this.timerCola4.Tick += new System.EventHandler(this.timerCola4_Tick);
            // 
            // timerCola5
            // 
            this.timerCola5.Enabled = true;
            this.timerCola5.Tick += new System.EventHandler(this.timerCola5_Tick);
            // 
            // timerCola6
            // 
            this.timerCola6.Enabled = true;
            this.timerCola6.Tick += new System.EventHandler(this.timerCola6_Tick);
            // 
            // timerCola7
            // 
            this.timerCola7.Enabled = true;
            this.timerCola7.Tick += new System.EventHandler(this.timerCola7_Tick);
            // 
            // timerCola8
            // 
            this.timerCola8.Enabled = true;
            this.timerCola8.Tick += new System.EventHandler(this.timerCola8_Tick);
            // 
            // Simulator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 651);
            this.Controls.Add(this.panel);
            this.Name = "Simulator";
            this.Text = "Simulator";
            this.Load += new System.EventHandler(this.Simulator_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer temporizador;
        private System.Windows.Forms.Panel panel;
        private System.Windows.Forms.Timer timerCola1;
        private System.Windows.Forms.Timer timerCola2;
        private System.Windows.Forms.Timer timerCola3;
        private System.Windows.Forms.Timer timerCola4;
        private System.Windows.Forms.Timer timerCola5;
        private System.Windows.Forms.Timer timerCola6;
        private System.Windows.Forms.Timer timerCola7;
        private System.Windows.Forms.Timer timerCola8;
    }
}

