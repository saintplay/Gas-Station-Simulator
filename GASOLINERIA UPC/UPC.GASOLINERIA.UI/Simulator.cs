﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UPC.GASOLINERIA.LIBRARY;

namespace UPC.GASOLINERIA.UI
{
    public partial class Simulator : Form
    {
        public int contador = -1;
        public int modelosCount;
        public Isla islaG1;
        public Isla islaG2;
        public Isla islaPe;
        public Isla islaGn;
        public List<int> listaDeEntradas;
        public Image[] carsImages;
        public Graphics graphics;
        public BufferedGraphics buffer;
        public BufferedGraphicsContext context;
        public Dibujos dibujos;

        public Simulator()
        {
            InitializeComponent();

            listaDeEntradas = new List<int>();

            modelosCount = Enum.GetNames(typeof(Modelos)).Length;

            carsImages = new Image[modelosCount];

            for (int i = 0; i < modelosCount; i++)
            {
                carsImages[i] = Image.FromFile("Data\\Images\\" + (Modelos.redCar + i).ToString() +  ".png"); 
            }

            islaG1 = new Isla(Combustibles.Gasolina, 1, 2, 300, 40); 
            islaG1.islaID = 1;
            islaG1.colaArriba.onAtencion += RefreshDatosIslaG1;
            islaG2 = new Isla(Combustibles.Gasolina, 3, 4, 300, 190);
            islaG2.islaID = 2;
            islaPe = new Isla(Combustibles.Petroleo, 5, 6, 300, 340);
            islaPe.islaID = 3;
            islaGn = new Isla(Combustibles.GNV, 7, 8, 300, 490);
            islaGn.islaID = 4;

            graphics = panel.CreateGraphics();
            context = BufferedGraphicsManager.Current;
			buffer = context.Allocate(graphics, this.ClientRectangle);;

            dibujos = new Dibujos(this);

            dibujos.PintarGrifo();

        }

        private void Simulator_Load(object sender, EventArgs e)
        {      
            
        }

        public ColaDeCarros getColaByID(int pID)
        {
            if (pID == 1)
                return islaG1.colaArriba;
            else if (pID == 2)
                return islaG1.colaAbajo;
            else if (pID == 3)
                return islaG2.colaArriba;
            else if (pID == 4)
                return islaG2.colaAbajo;
            else if (pID == 5)
                return islaPe.colaArriba;
            else if (pID == 6)
                return islaPe.colaAbajo;
            else if (pID == 7)
                return islaGn.colaArriba;
            else
                return islaGn.colaAbajo;
        }

        public Isla ElegirIsla(Carro pCarro, Random pAleatorio)
        {
            if (pCarro.combustible.tipo == Combustibles.Gasolina)
            {
                if (islaG1.retornartMinimoDeColas() < islaG2.retornartMinimoDeColas())
                    return islaG1;
                else if (islaG2.retornartMinimoDeColas() < islaG1.retornartMinimoDeColas())
                    return islaG2;
                else if (pAleatorio.Next(2) == 0)
                    return islaG1;
                else
                    return islaG2;
            }
            else if (pCarro.combustible.tipo == Combustibles.Petroleo)
                return islaPe;

            else
                return islaGn;
        }

        void RefreshDatosIslaG1()
        {
            islaG1.atualizarValores();

            dibujos.PintarIsla(islaG1, true);

        }

        private void temporizador_Tick(object sender, EventArgs e)
        {
            if(contador == -1)
                dibujos.PintarGrifo(true);

            contador++;
            Random aleatorio = new Random();

            if (contador % 80 == 0)
            {
                Carro carro = new Carro(aleatorio);
                
                Isla isla = ElegirIsla(carro, aleatorio);

                ColaDeCarros cola = isla.RetornarColaLibre();
                cola = islaG1.colaArriba;

                carro.colaID = cola.colaID;
                listaDeEntradas.Add(cola.colaID);


                cola.addCarro(carro);
                cola.estado = EstadosCola.Moviendose;

                            
            }

                contador++;

        }

        private void timerCola1_Tick(object sender, EventArgs e)
        {
          ColaDeCarros cola = islaG1.colaArriba;
          
          if (cola.estado != EstadosCola.Vacia)
          {
                cola.MoverCola();

                if (cola.estado == EstadosCola.Atendiendo)
                cola.AtenderCarro();

                dibujos.PintarCola(cola, true);
          }
            
        }

        private void timerCola2_Tick(object sender, EventArgs e)
        {

        }

        private void timerCola3_Tick(object sender, EventArgs e)
        {

        }


        private void timerCola4_Tick(object sender, EventArgs e)
        {

        }

        private void timerCola5_Tick(object sender, EventArgs e)
        {

        }

        private void timerCola6_Tick(object sender, EventArgs e)
        {

        }

        private void timerCola7_Tick(object sender, EventArgs e)
        {

        }

        private void timerCola8_Tick(object sender, EventArgs e)
        {

        }

    }
}
